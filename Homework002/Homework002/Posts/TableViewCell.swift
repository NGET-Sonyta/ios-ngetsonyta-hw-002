//
//  TableViewCell.swift
//  Homework002
//
//  Created by Nyta on 11/23/20.
//

import UIKit

class TableViewCell: UITableViewCell{

    
    //Connect Outlet of all UI elements in customized post cell
    @IBOutlet weak var past: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var ownerPf: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var totalLikes: UILabel!
    @IBOutlet weak var commentPf: UIImageView!
    @IBOutlet weak var commet: UITextField!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Set layer radius of pictures
        ownerPf.layer.cornerRadius = ownerPf.frame.height/2
        commentPf.layer.cornerRadius = commentPf.frame.height/2
        ownerPf.layer.masksToBounds = true
        ownerPf.layer.borderWidth = 2
        
        //Set black background color of user profile
        ownerPf.layer.borderColor = UIColor.black.cgColor
        
        //Set username property (font size to bold...)
        ownerName.font = UIFont.boldSystemFont(ofSize: ownerName.font.pointSize)
        name.font = UIFont.boldSystemFont(ofSize: name.font.pointSize)
        past.tintColor = .gray
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
}

