//
//  CollectionViewCell.swift
//  Homework002
//
//  Created by Nyta on 11/23/20.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var storyUsername: UILabel!
    @IBOutlet weak var storyImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //Set image layer radius
        storyImage.layer.cornerRadius = storyImage.frame.height/2
        storyImage.layer.masksToBounds = true
        
        //Set image layer border
        storyImage.layer.borderWidth = 2
        
        //Set image layer color to orange 
        storyImage.layer.borderColor = UIColor.orange.cgColor
    }

}
