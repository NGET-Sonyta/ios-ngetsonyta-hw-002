//
//  HomeViewController.swift
//  Homework002
//
//  Created by Nyta on 11/23/20.
//

import UIKit

class HomeViewController: UIViewController{

   //Connect Outlet to Collectin View
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Connect Outlet to Collectin View
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Tab Bar color to black when clicked
        UITabBar.appearance().tintColor = UIColor.black
        
        //Register Nib of Table View Cell
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.delegate = self
        tableView.dataSource = self
        
        //Register Nib of Collection View Cell
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cells")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //Disable selected function on posts
        tableView.allowsSelection = false
        
        //Hide horizontal scroll bar on stories
        collectionView.showsHorizontalScrollIndicator = false
        
    }
    
}

extension HomeViewController : UITableViewDelegate{}

extension HomeViewController : UITableViewDataSource{
    
    //Return number of posts
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell

        //Get object of username to display
        cell.ownerName.text = items[indexPath.row]["Name"]
        
        //Get object name to display
        cell.name.text = items[indexPath.row]["Name"]
        
        //Get object of like to display
        cell.totalLikes.text = items[indexPath.row]["Likes"]
        
        //Get object of captions to display
        cell.caption.text = items[indexPath.row]["Caption"]
        
        //Get object location to display
        cell.location.text = items[indexPath.row]["Location"]
        
        //Get object of duration (1mn ago) to display
        cell.past.text = items[indexPath.row]["Past"]
        
        //Get object of profile commenters to display as images
        let cmtPf = items[indexPath.row]["CommentPf"]!
        cell.commentPf?.image = UIImage(named: cmtPf)
        
        //Get object of profile pictures to display as images
        let pic = items[indexPath.row]["Profile"]!
        cell.ownerPf?.image = UIImage(named: pic)
        
        //Get object of images to display in each post
        let img = items[indexPath.row]["Image"]!
        cell.picture?.image = UIImage(named: img)
        
        //Set font size of "Like" to bold
        cell.totalLikes.font = UIFont.boldSystemFont(ofSize: cell.totalLikes.font.pointSize)
        
        return cell
    }
    
    //Define height of each row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 750
    }
}

extension HomeViewController : UICollectionViewDelegateFlowLayout{
    
    //Set Size of Collection View Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 500)
    }
    
}

extension HomeViewController : UICollectionViewDelegate{}

extension HomeViewController : UICollectionViewDataSource{
    
    //Return Number of stories
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return story.count
    }
    
    //Display each story in cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cells", for: indexPath) as! CollectionViewCell
        
        //Display username in story
        cell.storyUsername.text = story[indexPath.row]["Username"]
        
        //Display image in story's image
        let image = story[indexPath.row]["Image"]!
        cell.storyImage?.image = UIImage(named: image)
        
    return cell

    }
}

