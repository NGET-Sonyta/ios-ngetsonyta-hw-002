//
//  Objects.swift
//  Homework002
//
//  Created by Nyta on 11/23/20.
//

import Foundation

//Create post items data
let items: [[String: String]] = [

                                    ["Name": "cestlavie.kh",
                                     "Likes":"Liked by elf._.isfine and 1,355 others",
                                     "Caption": "🌻🐒 #ดาวินักท่องเที่ยว",
                                     "Image": "la",
                                     "Profile": "la",
                                     "CommentPf": "solikah",
                                     "Past" : "1 minute ago",
                                     "Location": "Phnom Penh"],
                                    ["Name": "naka_chea",
                                     "Likes":"Liked by virak_nich and 154,511 others ",
                                     "Caption": "I deserve all the respected , loved and trusted 🍊",
                                     "Image": "lyka",
                                     "Profile": "davika",
                                     "CommentPf": "yaya",
                                     "Past" : "3 minute ago",
                                     "Location": "Sponsor"],
                                    ["Name": "linlin_vee",
                                     "Likes":"Liked by jeleng__ and 6,125 others ",
                                     "Caption": " Always in the mood for you❤️",
                                     "Image": "davi",
                                     "Profile": "stella",
                                     "CommentPf": "davika",
                                     "Past" : "1 hour ago",
                                     "Location": "Osaka Japan"],
                                ]

//Create story data
let story : [[String: String]] = [
                                    ["Username": "s.dalinaa",
                                     "Image": "stella"],
                                    ["Username": "redly.s",
                                     "Image": "davika"],
                                    ["Username": "daa_mtd",
                                    "Image": "yaya"],
                                    ["Username": "hin_channiroth",
                                     "Image": "la"],
                                    ["Username": "fi.yata",
                                     "Image": "lyka"],
                                    ["Username": "melia.cstn",
                                    "Image": "solikah"],
                                 ]
